#!/bin/bash

helm install --name mariadb stable/mariadb \
  --set replication.enabled=false \
  --set rootUser.password=${ROOT_PASSWORD} \
  --set db.name=${DB_NAME} \
  --set db.user=${DB_USER} \
  --set db.password=${DB_PASSWORD} \
  --set master.persistence.size=2Gi \
  --set slave.persistence.size=2Gi \
  --debug
  
