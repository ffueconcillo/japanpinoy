#!/bin/bash

helm install ./flarum-helm/ --name flarum \
    --set env.flarum.forum_url=${FORUM_URL} \
    --set env.flarum.upload_maxsize=50M \
    --set env.flarum.admin_user=${ADMIN_USER} \
    --set env.flarum.admin_pass=${ADMIN_PASS} \
    --set env.flarum.admin_mail=${ADMIN_EMAIL} \
    --set env.flarum.title=${FORUM_TITLE} \
    --set env.database.host=${DB_HOST} \
    --set env.database.name=${DB_NAME} \
    --set env.database.user=${DB_USER} \
    --set env.database.pass=${DB_PASS} \
    --set env.database.prefix=${DB_PREFIX} \
    --set ingress.hosts=${HOST_NAMES}