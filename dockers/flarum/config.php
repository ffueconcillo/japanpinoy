<?php return array (
  'debug' => false, // enables or disables debug mode, used to troubleshoot issues
  'database' =>
  array (
    'driver' => 'mysql', // the database driver, i.e. MySQL, MariaDB...
    'host' => 'mariadb', // the host of the connection, localhost in most cases unless using an external service
    'database' => 'jp', // the name of the database in the instance
    'username' => 'jpuser', // database username
    'password' => 'weFV-wZ7+qVEzy?u', // database password
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix' => '', // the prefix for the tables, useful if you are sharing the same database with another service
    'port' => '3306', // the port of the connection, defaults to 3306 with MySQL
    'strict' => false,
  ),
  'url' => 'http://www.japanpinoy.com', // the URL installation, you will want to change this if you change domains
  'paths' =>
  array (
    'api' => 'api', // /api goes to the API
    'admin' => 'admin', // /admin goes to the admin
  ),
);